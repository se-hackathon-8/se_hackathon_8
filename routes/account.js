const express = require('express');
const router = express.Router();
const AccountModel = require("../models/account")

// @desc Getting all major
// @route GET /majors
router.get('/',async (req,res) => {
    try {
        const Account = await AccountModel.find();
        console.log("Get All Account Successful")
        res.status(200).json({data : Account})
    }catch (err){
        console.error(err.message)
        res.status(500).json({message : err.message})
        
    }
});

router.post('/login',async (req,res) => {
    try {
        const Account = await AccountModel.findOne({username : req.body.username , password : req.body.password});
        if(Account === null){
            res.status(500).json({message : "Cant Login"})
        }else{
            console.log("Login Successful")
            res.status(200).json({data : Account})
        }
    }catch (err){
        console.error(err.message)
        res.status(500).json({message : err.message})
    }
});

// @desc Getting by major Id
// @route GET /majors/:id
router.get('/:id',getAccount,async (req,res) => {
    console.log("Get Account by ID Successful")
    res.status(200).json({data : req.Account})
});

// @desc Creating one major
// @route POST /majors/:id
router.post('/',async (req,res) => {
    try {
        const newAccount = await AccountModel.create(req.body)
        console.log("Create Account Successful")
        res.status(201).json({data : newAccount})
    }catch (err){
        if(err.code === 11000){
            const keys = Object.keys(err.keyValue);
            err.message = `This ${keys.toString()} is already taken.`
        }
        console.error(err.message)
        res.status(500).json({message : err.message})
    }
})

// @desc Updating one major
// @route PUT /majors/:id
router.put('/:id',async (req,res) => {
    const AccountID = req.params.id
    try {
        const updatedAccount = await AccountModel.findByIdAndUpdate(AccountID,req.body,{
            new : true,
            runValidators : true
        });
        console.log("Update Account Successful")
        res.json({data : updatedAccount})
    }catch (err){
        console.error(err.message)
        res.status(400).json({message : err.message})
    }
})

// @desc Deleting one major
// @route DELETE /majors/:id
router.delete('/:id',getAccount,async (req,res) => {
    const AccountRemove = req.Account
    try {
        await AccountRemove.remove();
        console.log("Delete Account Successful")
        res.json({message : "Delete Account Successful",data : {}})
    }catch (err){
        res.status(500).json({message : err.message})
    }
})

//Validate Major(Middleware Functions)
async function getAccount(req,res,next) {
    let Account;
    try{
        Account = await AccountModel.findById(req.params.id);
        if (!Account){
            return res.status(404).json({message : "Cannot Find Account"})
        }
    }catch (err){
        //Bad ObjectID
        let message = err.message;
        let code = 500;
        if(err.name = "CastError"){
            message = "Data Not Found";
            code = 400;
        }
        console.error(err.message)
        return res.status(code).json({message : message})
    }
    req.Account = Account;
    next();
}

module.exports = router;