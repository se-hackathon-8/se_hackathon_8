const express = require('express');
const router = express.Router();
const VaccineModel = require("../models/vaccine")

// @desc Getting all major
// @route GET /majors
router.get('/',async (req,res) => {
    try {
        const vaccine = await VaccineModel.find();
        console.log("Get All Vaccine Successful")
        res.status(200).json({data : vaccine})
    }catch (err){
        console.error(err.message)
        res.status(500).json({message : err.message})
        
    }
});

// @desc Getting by major Id
// @route GET /majors/:id
router.get('/:id',getVaccine,async (req,res) => {
    console.log("Get Vaccine by ID Successful")
    res.status(200).json({data : req.vaccine})
});

// @desc Creating one major
// @route POST /majors/:id
router.post('/',async (req,res) => {
    try {
        console.log(req);
        const newVaccine = await VaccineModel.create(req.body)
        console.log("Create Vaccine Successful")
        res.status(201).json({data : newVaccine})
    }catch (err){
        if(err.code === 11000){
            const keys = Object.keys(err.keyValue);
            err.message = `This ${keys.toString()} is already taken.`
        }
        console.error(err.message)
        res.status(500).json({message : err.message})
    }
})

// @desc Updating one major
// @route PUT /majors/:id
router.put('/:id',async (req,res) => {
    const VaccineID = req.params.id
    try {
        const updatedvaccine = await VaccineModel.findByIdAndUpdate(vaccineID,req.body,{
            new : true,
            runValidators : true
        });
        console.log("Update Vaccine Successful")
        res.json({data : updatedvaccine})
    }catch (err){
        console.error(err.message)
        res.status(400).json({message : err.message})
    }
})

// @desc Deleting one major
// @route DELETE /majors/:id
router.delete('/:id',getVaccine,async (req,res) => {
    const VaccineRemove = req.covid
    try {
        await VaccineRemove.remove();
        console.log("Delete Vaccine Successful")
        res.json({message : "Delete Vaccine Successful",data : {}})
    }catch (err){
        res.status(500).json({message : err.message})
    }
})

//Validate Major(Middleware Functions)
async function getVaccine(req,res,next) {
    let vaccine;
    try{
        vaccine = await VaccineModel.findById(req.params.id);
        if (!vaccine){
            return res.status(404).json({message : "Cannot Find Vaccine"})
        }
    }catch (err){
        //Bad ObjectID
        let message = err.message;
        let code = 500;
        if(err.name = "CastError"){
            message = "Data Not Found";
            code = 400;
        }
        console.error(err.message)
        return res.status(code).json({message : message})
    }
    req.covid = vaccine;
    next();
}

module.exports = router;