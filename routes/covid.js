const express = require('express');
const router = express.Router();
const CovidModel = require("../models/covid")

// @desc Getting all major
// @route GET /majors
router.get('/',async (req,res) => {
    try {
        const covid = await CovidModel.find();
        console.log("Get All Covid Successful")
        res.status(200).json({data : covid})
    }catch (err){
        console.error(err.message)
        res.status(500).json({message : err.message})
        
    }
});

// @desc Getting by major Id
// @route GET /majors/:id
router.get('/:id',getCovid,async (req,res) => {
    console.log("Get Covid by ID Successful")
    res.status(200).json({data : req.covid})
});

// @desc Creating one major
// @route POST /majors/:id
router.post('/',async (req,res) => {
    try {
        const newCovid = await CovidModel.create(req.body)
        console.log("Create Covid Successful")
        res.status(201).json({data : newCovid})
    }catch (err){
        if(err.code === 11000){
            const keys = Object.keys(err.keyValue);
            err.message = `This ${keys.toString()} is already taken.`
        }
        console.error(err.message)
        res.status(500).json({message : err.message})
    }
})

// @desc Updating one major
// @route PUT /majors/:id
router.put('/:id',async (req,res) => {
    const covidID = req.params.id
    try {
        const updatedcovid = await CovidModel.findByIdAndUpdate(covidID,req.body,{
            new : true,
            runValidators : true
        });
        console.log("Update Covid Successful")
        res.json({data : updatedcovid})
    }catch (err){
        console.error(err.message)
        res.status(400).json({message : err.message})
    }
})

// @desc Deleting one major
// @route DELETE /majors/:id
router.delete('/:id',getCovid,async (req,res) => {
    const CovidRemove = req.covid
    try {
        await CovidRemove.remove();
        console.log("Delete Covid Successful")
        res.json({message : "Delete Covid Successful",data : {}})
    }catch (err){
        res.status(500).json({message : err.message})
    }
})

//Validate Major(Middleware Functions)
async function getCovid(req,res,next) {
    let covid;
    try{
        covid = await CovidModel.findById(req.params.id);
        if (!covid){
            return res.status(404).json({message : "Cannot Find Covid"})
        }
    }catch (err){
        //Bad ObjectID
        let message = err.message;
        let code = 500;
        if(err.name = "CastError"){
            message = "Data Not Found";
            code = 400;
        }
        console.error(err.message)
        return res.status(code).json({message : message})
    }
    req.covid = covid;
    next();
}

module.exports = router;