const express = require("express");
const routes = express.Router();

const CovidRoutes = require("./covid");
const AccRoutes = require("./account");
const VaccineRoutes = require("./vaccine");
const TreatmentRoutes = require("./treatment");

routes.use("/covid",CovidRoutes);
routes.use("/vaccine", VaccineRoutes);
routes.use("/treatment",TreatmentRoutes);
routes.use("/account",AccRoutes);

module.exports = routes;