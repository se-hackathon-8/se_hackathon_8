const express = require('express');
const router = express.Router();
const VaccineModel = require("../models/treatment")

// @desc Getting all major
// @route GET /majors
router.get('/',async (req,res) => {
    try {
        const treatment = await VaccineModel.find();
        console.log("Get All Treatment Successful")
        res.status(200).json({data : treatment})
    }catch (err){
        console.error(err.message)
        res.status(500).json({message : err.message})
        
    }
});

// @desc Getting by major Id
// @route GET /majors/:id
router.get('/:id',getTreatment,async (req,res) => {
    console.log("Get Treatment by ID Successful")
    res.status(200).json({data : req.treatment})
});

// @desc Creating one major
// @route POST /majors/:id
router.post('/',async (req,res) => {
    try {
        console.log(req);
        const newTreatment = await VaccineModel.create(req.body)
        console.log("Create Treatment Successful")
        res.status(201).json({data : newTreatment})
    }catch (err){
        if(err.code === 11000){
            const keys = Object.keys(err.keyValue);
            err.message = `This ${keys.toString()} is already taken.`
        }
        console.error(err.message)
        res.status(500).json({message : err.message})
    }
})

// @desc Updating one major
// @route PUT /majors/:id
router.put('/:id',async (req,res) => {
    const TreatmentID = req.params.id
    try {
        const updatedtreatment = await VaccineModel.findByIdAndUpdate(treatmentID,req.body,{
            new : true,
            runValidators : true
        });
        console.log("Update Treatment Successful")
        res.json({data : updatedvaccine})
    }catch (err){
        console.error(err.message)
        res.status(400).json({message : err.message})
    }
})

// @desc Deleting one major
// @route DELETE /majors/:id
router.delete('/:id',getTreatment,async (req,res) => {
    const TreatmentRemove = req.covid
    try {
        await TreatmentRemove.remove();
        console.log("Delete Treatment Successful")
        res.json({message : "Delete Treatment Successful",data : {}})
    }catch (err){
        res.status(500).json({message : err.message})
    }
})

//Validate Major(Middleware Functions)
async function getTreatment(req,res,next) {
    let treatment;
    try{
        treatment = await VaccineModel.findById(req.params.id);
        if (!treatment){
            return res.status(404).json({message : "Cannot Find Treatment"})
        }
    }catch (err){
        //Bad ObjectID
        let message = err.message;
        let code = 500;
        if(err.name = "CastError"){
            message = "Data Not Found";
            code = 400;
        }
        console.error(err.message)
        return res.status(code).json({message : message})
    }
    req.treatment = treatment;
    next();
}

module.exports = router;