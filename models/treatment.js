const mongoose = require("mongoose");

const Treatment = new mongoose.Schema({
  
    date : {
        type : Date,
        required : true,
    },
    hospital : {
        type : String,
        required : true,
        unique : true
    },
    cause : {
        type : String,
        required : true,
        unique : true
    },
 
})

module.exports = mongoose.model('Tretment', Treatment)