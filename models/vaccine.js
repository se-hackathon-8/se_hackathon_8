const mongoose = require("mongoose");

const Vaccine = new mongoose.Schema({
  
    vaccineName : {
        type : String,
        required : true,
    },
    date : {
        type : Date,
        required : true,
    },
    lotNo : {
        type : Number,
        required : true,
        
    }, 
    serialNo : {
        type : String,
        required : true,
        unique : true
    },
    hospital : {
        type : String,
        required : true,
        unique : true
    },
 
})

module.exports = mongoose.model('Vaccine', Vaccine)