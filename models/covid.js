const mongoose = require("mongoose");

const Covid = new mongoose.Schema({
    firstName : {
        type : String,
        required : true,
    },
    lastName : {
        type : String,
        required : true,
    },
    id : {
        type : String,
        required : true,
        unique : true
    },
    username : {
        type : String,
        required : true,
        unique : true
    }, 
    password : {
        type : String,
        required : true,
    },
    password : {
        type : String,
        required : true,
        unique : true
    },
    phonenumber: {
        type : String,
        required : true,
    },
    birthday : {
        type : String,
        required : true,
    },
   
})

module.exports = mongoose.model('Covid', Covid)